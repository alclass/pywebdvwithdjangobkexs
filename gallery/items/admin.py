from items.models import Item
from items.models import Photo
from django.contrib import admin

class PhotoInline(admin.StackedInline):
    model = Photo

class ItemAdmin(admin.ModelAdmin):
    inlines = [PhotoInline]

admin.site.register(Item, ItemAdmin) #Item    
admin.site.register(Photo)
