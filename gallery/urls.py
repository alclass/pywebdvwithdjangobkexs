from django.conf.urls.defaults import *
#from gallery.settings import ROOT_URL
from django.contrib import admin
admin.autodiscover()
#import items.urls

urlpatterns = patterns('',
  url(r'^', include('gallery.items.urls')),
  url(r'^admin/', include(admin.site.urls)),
  #url(r'^', include('gallery.real_urls')), # r'^%s' % ROOT_URL[1:]
)
