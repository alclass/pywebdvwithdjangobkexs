from django.conf.urls.defaults import *
from django.contrib.syndication.views import feed
from BlogApp.views import archive
from BlogApp.feeds import RSSFeed
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', include('BlogApp.urls')),
)
