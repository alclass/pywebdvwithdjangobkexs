from django.conf.urls.defaults import *
from django.contrib.syndication.views import feed
from BlogApp.views import archive
from BlogApp.feeds import RSSFeed

urlpatterns = patterns('',
    url(r'^$', archive),
    url(r'^feeds/(?P<url>.*)/$', feed, {'feed_dict': {'rss': RSSFeed}}),
)
